package edu.miu.cs.cs544.group5.appointment.service;

import java.util.List;

import edu.miu.cs.cs544.group5.appointment.domain.Reservation;
import edu.miu.cs.cs544.group5.appointment.domain.User;

public interface ReservationService {
    public Reservation createReservation(Reservation reservation, long appointmentId);
    public Boolean cancelReservation(long reservationId);
    public List<Reservation> getAllReservations();
    public List<Reservation> getSortedReservations();
    public Reservation updateReservation(Reservation reservation,long reservationId);
    public Reservation getReservationById(long reservationId);

    public List<Reservation> getAllReservationsByUserAndAppointment(User user, long appointmentId);
    public Reservation updateReservationByUserAndAppointment(User user, long appointmentId, long reservationId, Reservation.Status status);
}
