package edu.miu.cs.cs544.group5.appointment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import edu.miu.cs.cs544.group5.appointment.domain.Email;

@Repository
public interface EmailRepository extends JpaRepository<Email, Long>{
    
    @Modifying
    @Query("UPDATE Email e SET e.status = :status WHERE e.id = :id")
    int setStatus(Email.Status status, Long id);
}
