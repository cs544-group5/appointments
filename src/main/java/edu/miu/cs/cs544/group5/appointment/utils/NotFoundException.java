package edu.miu.cs.cs544.group5.appointment.utils;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(Class<?> cls, long id) {
        super(String.format("Entity %s with id %d not found", cls.getSimpleName(), id));
    }

    public NotFoundException(Class<?> cls, String id) {
        super(String.format("Entity %s with id %s not found", cls.getSimpleName(), id));
    }
}
