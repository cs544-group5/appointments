package edu.miu.cs.cs544.group5.appointment.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.miu.cs.cs544.group5.appointment.domain.User;
import edu.miu.cs.cs544.group5.appointment.dto.JwtAuthenticationResponse;
import edu.miu.cs.cs544.group5.appointment.dto.LoginRequests;
import edu.miu.cs.cs544.group5.appointment.security.JwtTokenProvider;
import edu.miu.cs.cs544.group5.appointment.security.UserPrincipal;
import edu.miu.cs.cs544.group5.appointment.service.UserService;

@RestController
@CrossOrigin
@RequestMapping("/api/auth/")
public class AuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@RequestBody LoginRequests loginRequest) {
        System.out.println("Logging");
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, (UserPrincipal) authentication.getPrincipal()));
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/check")
    public ResponseEntity<?> checkSecurity(Authentication authentication) {
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        User user = principal.getUser();
        String msg = String.format("Client Check Okay! Admin=%s Provider=%s Client=%s", user.isAdmin(), user.isProvider(), user.isClient());
        return ResponseEntity.ok(msg);
    }

    @PostMapping("/signup")
    public ResponseEntity<?> signup(@RequestBody User user) {
        User newUser = userService.registerNewUserAccount(user);
        return ResponseEntity.ok(UserPrincipal.create(newUser));
    }
}
