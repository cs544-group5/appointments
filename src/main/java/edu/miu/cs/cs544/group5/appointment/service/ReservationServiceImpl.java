package edu.miu.cs.cs544.group5.appointment.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import edu.miu.cs.cs544.group5.appointment.domain.Appointment;
import edu.miu.cs.cs544.group5.appointment.domain.Reservation;
import edu.miu.cs.cs544.group5.appointment.domain.User;
import edu.miu.cs.cs544.group5.appointment.repository.AppointmentRepository;
import edu.miu.cs.cs544.group5.appointment.repository.ReservationRepository;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @Autowired
    private EmailService emailService;

    @Override
    public Reservation createReservation(Reservation reservation, long appointmentId) {
        Appointment appointment = appointmentRepository.getById(appointmentId);
        reservation.setAppointment(appointment);
        reservation.setStatus(Reservation.Status.PENDING);
        return reservationRepository.save(reservation);
    }
    @Override
    public Boolean cancelReservation(long reservationId) {

        if(hasReservation(reservationId)){
            Reservation reservation = reservationRepository.getById(reservationId);
            reservationRepository.delete(reservation);
            return true;
        }
        return false;
    }

    @Override
    public List<Reservation> getAllReservations() {
        return reservationRepository.findAll();
    }

    //get sorted reservations by date
    @Override
    public List<Reservation> getSortedReservations() {
        return reservationRepository.findAll(Sort.by(Sort.Direction.DESC,"date"));
    }

    @Override
    public Reservation updateReservation(Reservation reservation,long reservationId) {
        if(!reservation.getStatus().equals(Reservation.Status.DECLINED) && hasReservation(reservationId)){

            Reservation updatedReservation = reservationRepository.findById(reservationId).orElseThrow(null);

            updatedReservation.setAppointment(reservation.getAppointment());
//            updatedReservation.setDate(reservation.getCurrentDate());

            return reservationRepository.save(updatedReservation);
        }
        return null;
    }

    @Override
    public Reservation getReservationById(long reservationId) {
        if(hasReservation(reservationId)){
            return reservationRepository.getById(reservationId);
        }
        return null;
    }

    //a method to check whether the data is found or not
    public boolean hasReservation(long id){
        if (reservationRepository.findById(id).isPresent()){
            return true;
        }
        return false;
    }
    @Override
    public List<Reservation> getAllReservationsByUserAndAppointment(User user, long appointmentId) {
        Appointment appointment = appointmentRepository.findById(appointmentId).get();
        if (user.isAdmin() || (appointment.getUser().getId() == user.getId())) {
            return reservationRepository.findByAppointment(appointment);
        }
        return new ArrayList<>();
    }

    @Override
    public Reservation updateReservationByUserAndAppointment(User user, long appointmentId, long reservationId, Reservation.Status status) {
        Appointment appointment = appointmentRepository.findById(appointmentId).get();
        if (!user.isAdmin() && appointment.getUser().getId() != user.getId()) {
            return null; // user is not admin or doesn't own the appointment
        }
        Reservation reservation = reservationRepository.findById(reservationId).get();
        if (reservation.getAppointment().getId() != appointment.getId()) {
            return null; // reservation is not part of appointment
        }
        if (status == Reservation.Status.ACCEPTED && appointment.getAcceptedReservation().isPresent()) {
            return null;  // duplicate accept
        }
        // everything is okay
        reservation.setStatus(status);
        reservationRepository.save(reservation);
        // send email
        if (status.equals(Reservation.Status.ACCEPTED)) {
            emailService.sendAcceptEmail(appointment, reservation);
        } else if (status.equals(Reservation.Status.DECLINED)){
            emailService.sendDeclineEmail(appointment, reservation);
        }
        return reservation;
    }

}
