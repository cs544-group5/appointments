package edu.miu.cs.cs544.group5.appointment.domain;

import java.util.Collection;
import javax.persistence.*;

import lombok.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SecondaryTable(name = "accounts")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(table = "accounts", unique = true)
    private String email;

    @Column(table = "accounts")
    private String password;

    private String firstName;

    private String lastName;

    private String gender;

    @ElementCollection
    private Collection<UserRole> roles;

    public boolean isAdmin() {
        return roles.stream().filter(r -> r.getRole().equals(UserRole.Role.ADMIN)).findAny().isPresent();
    }

    public boolean isProvider() {
        return roles.stream().filter(r -> r.getRole().equals(UserRole.Role.PROVIDER)).findAny().isPresent();
    }

    public boolean isClient() {
        return roles.stream().filter(r -> r.getRole().equals(UserRole.Role.CLIENT)).findAny().isPresent();
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }
}
