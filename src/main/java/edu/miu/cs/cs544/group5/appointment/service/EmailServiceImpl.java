package edu.miu.cs.cs544.group5.appointment.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Map;
import java.util.TimeZone;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.miu.cs.cs544.group5.appointment.domain.Appointment;
import edu.miu.cs.cs544.group5.appointment.domain.Email;
import edu.miu.cs.cs544.group5.appointment.domain.Reservation;
import edu.miu.cs.cs544.group5.appointment.domain.User;
import edu.miu.cs.cs544.group5.appointment.dto.EmailDTO;
import edu.miu.cs.cs544.group5.appointment.repository.AppointmentRepository;
import edu.miu.cs.cs544.group5.appointment.repository.EmailRepository;

@Service
@Transactional
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    ModelMapper modelMapper;
    
    @Autowired
    private EmailRepository emailRepository;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @Autowired
    private CircuitBreakerFactory circuitBreakerFactory;

    @Value("${app.message.queue}")
    private String queue;

    @Value("#{${app.message.reminders}}")
    private Map<String, Integer> reminders;

    /**
     * Send reminder email for both provider and client
     */
    @Override
    public void sendReminderEmail(Appointment appointment, String reminder) {
        System.out.println("Sending reminder email for " + reminder + "...");
        User provider = appointment.getUser();
        User client = appointment.getAcceptedReservation().get().getUser();
        // send email to both provider & client
        String appointmentTime = appointment.getDate().atZone(TimeZone.getDefault().toZoneId()).format(DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss z"));
        String subject = String.format("[Reminder] Your %s appointment is on %s", appointment.getCategory().getTitle(),
                appointmentTime);
        Email providerEmail = new Email();
        providerEmail.setTo(provider.getEmail());
        providerEmail.setSubject(subject);
        providerEmail.setBody(String.format("Hello %s, your appointment with %s is on %s.", provider.getFullName(),
                client.getFullName(), appointmentTime));
        providerEmail.setReminder(reminder);
        providerEmail.setAppointment(appointment);
        Email clientEmail = new Email();
        clientEmail.setTo(client.getEmail());
        clientEmail.setSubject(subject);
        clientEmail.setBody(String.format("Hello %s, your appointment with %s is on %s.", client.getFullName(),
                provider.getFullName(), appointmentTime));
        clientEmail.setReminder(reminder);
        clientEmail.setAppointment(appointment);
        // save
        emailRepository.save(providerEmail);
        emailRepository.save(clientEmail);
        // send
        jmsTemplate.convertAndSend(queue, modelMapper.map(providerEmail, EmailDTO.class));
        jmsTemplate.convertAndSend(queue, modelMapper.map(clientEmail, EmailDTO.class));
    }

    /**
     * Send accept email for client
     */
    @Override
    public void sendAcceptEmail(Appointment appointment, Reservation reservation) {
        User client = reservation.getUser();
        User provider = appointment.getUser();
        String appointmentTime = appointment.getDate().atZone(TimeZone.getDefault().toZoneId()).format(DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss z"));
        String subject = String.format("[Accepted] Your %s appointment", appointment.getCategory().getTitle());

        Email clientEmail = new Email();
        clientEmail.setTo(client.getEmail());
        clientEmail.setSubject(subject);
        clientEmail.setBody(String.format("Hello %s, your appointment with %s on %s is accepted.", client.getFullName(),
                provider.getFullName(), appointmentTime));
        clientEmail.setReminder("accepted");
        clientEmail.setAppointment(appointment);
        emailRepository.save(clientEmail);
        jmsTemplate.convertAndSend(queue, modelMapper.map(clientEmail, EmailDTO.class));
    }

    /**
     * Send decline email for client
     */
    @Override
    public void sendDeclineEmail(Appointment appointment, Reservation reservation) {
        User client = reservation.getUser();
        User provider = appointment.getUser();
        String appointmentTime = appointment.getDate().atZone(TimeZone.getDefault().toZoneId()).format(DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss z"));
        String subject = String.format("[Declined] Your %s appointment", appointment.getCategory().getTitle());

        Email clientEmail = new Email();
        clientEmail.setTo(client.getEmail());
        clientEmail.setSubject(subject);
        clientEmail.setBody(String.format("Hello %s, your appointment with %s on %s is declined.", client.getFullName(),
                provider.getFullName(), appointmentTime));
        clientEmail.setReminder("declined");
        clientEmail.setAppointment(appointment);
        emailRepository.save(clientEmail);
        jmsTemplate.convertAndSend(queue, modelMapper.map(clientEmail, EmailDTO.class));
    }

    /**
     * Sends reminder email for appointments
     */
    @Scheduled(cron = "${app.message.schedule}")
    public void sendReminder() {
        System.out.println("Checking for sending reminder emails...");
        for (Map.Entry<String, Integer> entry : reminders.entrySet()) {
            String reminder = entry.getKey();
            Integer beforeMinute = entry.getValue();
            Collection<Appointment> appointments = appointmentRepository.findByDateAndHasNoReminder(LocalDateTime.now().plus(beforeMinute, ChronoUnit.MINUTES), reminder);
            for (Appointment appointment : appointments) {
                if (appointment.getAcceptedReservation().isPresent()) {
                    sendReminderEmail(appointment, reminder);
                }
            }
        }
    }

    /**
     * Listen for incoming request messages
     */
    @JmsListener(destination = "${app.message.queue}", containerFactory = "myFactory")
    public void receiveMessage(EmailDTO email) throws InterruptedException {
        CircuitBreaker circuitBreaker = circuitBreakerFactory.create("circuitBreaker");
        boolean success = circuitBreaker.run(
            () -> sendMail(email),
            throwable -> sendMailFallback(throwable, email)
        );
        if (success) {
            emailRepository.setStatus(Email.Status.SENT, email.getId());
        } else {
            emailRepository.setStatus(Email.Status.FAILED, email.getId());
        }
    }

    /**
     * Sends email via SMPT
     */
    public boolean sendMail(EmailDTO email) {
        System.out.println("Sending email \n" + email);
        MimeMessagePreparator preparator = mimeMessage -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
            message.setTo(email.getTo());
            message.setSubject(email.getSubject());
            message.setText(email.getBody(), false);
        };
        mailSender.send(preparator);
        System.out.println("Finished sending email!");
        return true;
    }

    /**
     * Handle email failures
     */
    private boolean sendMailFallback(Throwable throwable, EmailDTO email) {
        System.out.println("Email failed! error = " + throwable);
        // Do fallback logic here
        return false;
    }
}
