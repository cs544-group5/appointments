package edu.miu.cs.cs544.group5.appointment.dto;

import lombok.Data;
import lombok.Getter;

@Data
@Getter
public class LoginRequests {

    private String username;
    private String password;

}
