package edu.miu.cs.cs544.group5.appointment.domain;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime date;

    private String location;

    private Long duration;

    @ManyToOne(fetch = FetchType.EAGER)
    private Category category;

    @OneToMany(cascade=CascadeType.REMOVE, mappedBy = "appointment")
    @JsonManagedReference
    private Collection<Reservation> reservations;

    @JsonIgnore
    @ManyToOne
    private User user;

    @JsonIgnore
    @OneToMany(mappedBy = "appointment")
    private Collection<Email> emails;

    @JsonIgnore
    public Optional<Reservation> getAcceptedReservation() {
        if (this.reservations == null) {
            return Optional.empty();
        }
        return this.reservations.stream().filter(r -> r.getStatus().equals(Reservation.Status.ACCEPTED)).findFirst();
    }
}
