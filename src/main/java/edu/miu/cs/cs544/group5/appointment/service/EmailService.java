package edu.miu.cs.cs544.group5.appointment.service;

import edu.miu.cs.cs544.group5.appointment.domain.Appointment;
import edu.miu.cs.cs544.group5.appointment.domain.Reservation;

public interface EmailService {
    void sendReminderEmail(Appointment appointment, String reminder);
    void sendAcceptEmail(Appointment appointment, Reservation reservation);
    void sendDeclineEmail(Appointment appointment, Reservation reservation);
}
