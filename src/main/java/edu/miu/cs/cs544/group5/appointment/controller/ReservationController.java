package edu.miu.cs.cs544.group5.appointment.controller;

import edu.miu.cs.cs544.group5.appointment.dto.ReservationDTO;
import edu.miu.cs.cs544.group5.appointment.security.UserPrincipal;
import edu.miu.cs.cs544.group5.appointment.domain.Appointment;
import edu.miu.cs.cs544.group5.appointment.domain.Reservation;
import edu.miu.cs.cs544.group5.appointment.service.AppointmentService;
import edu.miu.cs.cs544.group5.appointment.service.ReservationService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/appointments/{appointmentID}/reservations")
public class ReservationController {
    @Autowired
    ReservationService reservationService;

    @Autowired
    AppointmentService appointmentService;

    @Autowired
    ModelMapper modelMapper;

    @PostMapping
    public ResponseEntity<?> createReservation(Authentication authentication, @PathVariable long appointmentID) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        Reservation newReservation = new Reservation();
        newReservation.setUser(userPrincipal.getUser());
        Reservation reservation = reservationService.createReservation(newReservation, appointmentID);
        // TODO Converting to DTO is failing
        // ReservationDTO reservationResponse = modelMapper.map(reservation, ReservationDTO.class);
        return new ResponseEntity<>(reservation, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'PROVIDER')")
    @GetMapping
    public ResponseEntity<List<ReservationDTO>> getAllReservations(Authentication authentication, @PathVariable long appointmentID) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        List<Reservation> reservationList = reservationService.getAllReservationsByUserAndAppointment(userPrincipal.getUser(), appointmentID);
        List<ReservationDTO> reservationDTOList = reservationList.stream()
                .map(reservation -> modelMapper.map(reservation, ReservationDTO.class)).collect(Collectors.toList());
        return new ResponseEntity<List<ReservationDTO>>(reservationDTOList, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'PROVIDER')")
    @PatchMapping("/{reservationId}")
    public ResponseEntity<?> updateReservation(Authentication authentication, @PathVariable long appointmentID, @PathVariable long reservationId, @RequestBody ReservationDTO reservationDTO) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        Reservation reservation = reservationService.updateReservationByUserAndAppointment(userPrincipal.getUser(), appointmentID, reservationId, reservationDTO.getStatus());
        if (reservation == null) {
            return ResponseEntity.badRequest().build();
        }
        ReservationDTO reservationResponse = modelMapper.map(reservation, ReservationDTO.class);
        return ResponseEntity.ok(reservationResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ReservationDTO> getReservationById(@RequestParam long reservationId) {
        Reservation reservation = reservationService.getReservationById(reservationId);

        ReservationDTO responseReservation = modelMapper.map(reservation, ReservationDTO.class);

        return new ResponseEntity<>(responseReservation, HttpStatus.FOUND);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ReservationDTO> updateReservation(@RequestBody ReservationDTO reservationDTO,
            @RequestParam long reservationId) {
        Reservation requestReservation = modelMapper.map(reservationDTO, Reservation.class);

        Reservation reservation = reservationService.updateReservation(requestReservation, reservationId);

        ReservationDTO responseReservation = modelMapper.map(reservation, ReservationDTO.class);

        return new ResponseEntity<>(responseReservation, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public String cancelReservation(@RequestParam long reservationId) {
        reservationService.cancelReservation(reservationId);
        return "Your reservation is cancelled";
    }
}
