package edu.miu.cs.cs544.group5.appointment.dto;

import edu.miu.cs.cs544.group5.appointment.domain.Reservation;
import lombok.Data;

@Data
public class ReservationDTO {
    private long Id;
    private Reservation.Status status;
}
