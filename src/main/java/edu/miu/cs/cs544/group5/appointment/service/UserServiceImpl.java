package edu.miu.cs.cs544.group5.appointment.service;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import edu.miu.cs.cs544.group5.appointment.domain.User;
import edu.miu.cs.cs544.group5.appointment.domain.UserRole;
import edu.miu.cs.cs544.group5.appointment.repository.UserRepository;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository repository;

    @Override
    public User registerNewUserAccount(User newUser) {
        newUser.setPassword(passwordEncoder.encode(newUser.getPassword())); // encrypt
        return repository.save(newUser);
    }

}
