package edu.miu.cs.cs544.group5.appointment.service;

import edu.miu.cs.cs544.group5.appointment.domain.User;

public interface UserService {
    User registerNewUserAccount(User accountDto);
}
