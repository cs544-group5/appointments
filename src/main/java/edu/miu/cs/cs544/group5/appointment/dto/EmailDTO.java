package edu.miu.cs.cs544.group5.appointment.dto;

import lombok.Data;

@Data
public class EmailDTO {
    private long id;
    private String to;
    private String body;
    private String subject;
}
