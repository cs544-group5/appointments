package edu.miu.cs.cs544.group5.appointment.repository;

import java.time.LocalDateTime;
import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.miu.cs.cs544.group5.appointment.domain.Appointment;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {
    public Collection<Appointment> findByCategoryId(Long id);
    
    @Query("FROM Appointment a WHERE a.date <= :date AND a NOT IN (SELECT e.appointment FROM Email e WHERE e.reminder = :reminder)")
    public Collection<Appointment> findByDateAndHasNoReminder(LocalDateTime date, String reminder);
}
