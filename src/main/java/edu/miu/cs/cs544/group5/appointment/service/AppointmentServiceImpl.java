package edu.miu.cs.cs544.group5.appointment.service;

import java.util.Collection;
import java.util.Optional;
import edu.miu.cs.cs544.group5.appointment.domain.Category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.miu.cs.cs544.group5.appointment.domain.Appointment;
import edu.miu.cs.cs544.group5.appointment.repository.AppointmentRepository;
import edu.miu.cs.cs544.group5.appointment.repository.CategoryRepository;
import edu.miu.cs.cs544.group5.appointment.utils.NotFoundException;

@Service
@Transactional
public class AppointmentServiceImpl implements AppointmentService {

    @Autowired
    private AppointmentRepository appointmentRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Appointment addAppointment(Appointment appointment) {
        Category category = null;

        if (appointment.getDuration() == null) {
            category = categoryRepository.findById(appointment.getCategory().getId())
                    .orElseThrow(() -> new NotFoundException(appointment.getCategory().getClass(),
                            appointment.getCategory().getId()));

            appointment.setDuration(category.getDuration());
        }

        Appointment newAppointment = appointmentRepository.save(appointment);

        newAppointment.setCategory(category);

        return newAppointment;
    }

    @Override
    public Appointment updateAppointment(Appointment updateData) {
        return appointmentRepository.save(updateData);
    }

    @Override
    public Collection<Appointment> getAppointments() {
        return appointmentRepository.findAll();
    }

    @Override
    public Collection<Appointment> getAppointmentsByCategoryId(Long id) {
        try {
            return appointmentRepository.findByCategoryId(id);
        } catch (RuntimeException e) {
            throw new NotFoundException(new Category().getClass(), id);
        }
    }

    @Override
    public Optional<Appointment> findAppointmentById(Long id) {
        try {
            return appointmentRepository.findById(id);
        } catch (RuntimeException e) {
            throw new NotFoundException(new Appointment().getClass(), id);
        }
    }

    @Override
    public void deleteAppointment(Long id) {
        try {
            appointmentRepository.deleteById(id);
        } catch (RuntimeException e) {
            throw new NotFoundException(new Appointment().getClass(), id);
        }
    }
}
