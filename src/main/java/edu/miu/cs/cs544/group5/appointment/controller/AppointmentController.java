package edu.miu.cs.cs544.group5.appointment.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.miu.cs.cs544.group5.appointment.domain.Appointment;
import edu.miu.cs.cs544.group5.appointment.security.UserPrincipal;
import edu.miu.cs.cs544.group5.appointment.service.AppointmentService;

@RestController
@RequestMapping("/appointments")
public class AppointmentController {

    @Autowired
    AppointmentService appointmentService;

    @GetMapping
    public Collection<Appointment> getAppointments(@RequestParam(required = false) Long categoryId) {
        System.out.println(categoryId);

        if(categoryId == null) {
            return appointmentService.getAppointments();
        }

        return appointmentService.getAppointmentsByCategoryId(categoryId);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'PROVIDER')")
    @PostMapping
    public Appointment saveAppointment(Authentication authentication, @RequestBody Appointment appointment) {
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        appointment.setUser(userPrincipal.getUser());
        return appointmentService.addAppointment(appointment);
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'PROVIDER')")
    @PutMapping("/{id}")
    public ResponseEntity<?> updateAppointment(Authentication authentication, @PathVariable Long id, @RequestBody Appointment appointment) {
        if (id.equals(appointment.getId())) {
            if (!appointmentService.findAppointmentById(id).isPresent()) {
                return ResponseEntity.badRequest().build();
            }

            UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
            appointment.setUser(userPrincipal.getUser());

            return ResponseEntity.ok(appointmentService.updateAppointment(appointment));
        }

        return ResponseEntity.badRequest().build();
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'PROVIDER')")
    @DeleteMapping("/{id}")
    public void deleteAppointment(@PathVariable Long id) {
        appointmentService.deleteAppointment(id);
    }
}
