package edu.miu.cs.cs544.group5.appointment.service;

import edu.miu.cs.cs544.group5.appointment.domain.Appointment;
import edu.miu.cs.cs544.group5.appointment.domain.Category;
import edu.miu.cs.cs544.group5.appointment.repository.AppointmentRepository;
import edu.miu.cs.cs544.group5.appointment.repository.CategoryRepository;
import java.util.Collection;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

  @Autowired
  private CategoryRepository categoryRepository;

  @Override
  public Category addCategory(Category category) {
    return categoryRepository.save(category);
  }

  @Override
  public Collection<Category> allCategories() {
    return categoryRepository.findAll();
  }

  @Override
  public Category findCategoryById(Long id) throws NotFoundException {
    return categoryRepository.findById(id).orElseThrow(NotFoundException::new);
  }

  @Override
  public Category updateCategory(Category category) {
    return categoryRepository.save(category);
  }
}
