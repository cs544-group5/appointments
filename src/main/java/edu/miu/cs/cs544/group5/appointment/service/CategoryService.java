package edu.miu.cs.cs544.group5.appointment.service;

import edu.miu.cs.cs544.group5.appointment.domain.Appointment;
import edu.miu.cs.cs544.group5.appointment.domain.Category;
import java.util.Collection;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;

public interface CategoryService {
  Category addCategory(Category category);
  Collection<Category> allCategories();
  // Collection<Appointment> allAppointments(Long id);
  Category findCategoryById(Long id) throws NotFoundException;
  Category updateCategory(Category category);
}
