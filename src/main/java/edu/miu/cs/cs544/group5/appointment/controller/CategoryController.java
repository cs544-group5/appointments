package edu.miu.cs.cs544.group5.appointment.controller;

import edu.miu.cs.cs544.group5.appointment.domain.Appointment;
import edu.miu.cs.cs544.group5.appointment.domain.Category;
import edu.miu.cs.cs544.group5.appointment.service.CategoryService;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@PreAuthorize("hasAnyAuthority('ADMIN')")
@RequestMapping("/categories")
public class CategoryController {

  @Autowired
  private CategoryService categoryService;

  @GetMapping
  public Collection<Category> getCategories() {
    return categoryService.allCategories();
  }

  @PostMapping
  public ResponseEntity<?> addCategory(@RequestBody Category category){
    return ResponseEntity.ok(categoryService.addCategory(category));
  }
  
  @PutMapping("/{id}")
  public ResponseEntity<?> updateCategory(@PathVariable Long id, Category category) {
    try {
      categoryService.findCategoryById(id);
    } catch (NotFoundException e) {
      return ResponseEntity.badRequest().build();
    }

    return ResponseEntity.ok(categoryService.updateCategory(category));
  }

}
