package edu.miu.cs.cs544.group5.appointment.service;

import edu.miu.cs.cs544.group5.appointment.domain.Appointment;
import java.util.Collection;
import java.util.Optional;

public interface AppointmentService {
  Appointment addAppointment(Appointment appointment);

  Appointment updateAppointment(Appointment updateData);

  Collection<Appointment> getAppointments();

  Optional<Appointment> findAppointmentById(Long id);

  void deleteAppointment(Long id);
  
  Collection<Appointment> getAppointmentsByCategoryId(Long Id);
}
