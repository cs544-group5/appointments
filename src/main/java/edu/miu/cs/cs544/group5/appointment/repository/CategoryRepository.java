package edu.miu.cs.cs544.group5.appointment.repository;

import edu.miu.cs.cs544.group5.appointment.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}